
-- Reimplementation of table.sort as a workaround
-- for C stack overflow errors
table.oldSort = table.sort
function table:sort(cmpFunc, startIndex, lastIndex)
    -- Init
    if cmpFunc == nil then
        table.oldSort(self)
        return
        -- cmpFunc = function(a,b) 
        --     return a < b
        -- end
    end

    if startIndex == nil then
        startIndex = 1
    end

    if lastIndex == nil then
        lastIndex = #self
    end

    table._sort(self, cmpFunc, startIndex, lastIndex)
end

function table:_sort(cmpFunc, startIndex, lastIndex)
    wall = startIndex

    if lastIndex - startIndex < 1 then
        -- Nothing to sort
        return
    end

    -- Moving median value to the back to avoid bad performance when sorting an already sorted array
    table.switchValues(self, math.ceil( (startIndex + lastIndex) / 2), lastIndex)

    -- Splitting the array according to comparisons with the last value
    for i = startIndex, lastIndex, 1 do
        if cmpFunc(self[i], self[lastIndex]) then
            table.switchValues(self, i, wall)
            wall = wall + 1
        end
    end

	-- Placing last value between the two split arrays
    table.switchValues(self, wall, lastIndex)

    -- Sorting left of the wall
	table.sort(self, cmpFunc, startIndex, wall - 1)

    -- Sorting right of the wall
	table.sort(self, cmpFunc, wall + 1, lastIndex)
end

function table:switchValues(firstIndex, secondIndex)
    if firstIndex ~= secondIndex then
        self[firstIndex], self[secondIndex] = self[secondIndex], self[firstIndex]
    end
end




function print_r(table)
    for i, v in pairs(table) do
        print(i,v)
    end
end


tabel = {'one', 'two', 'three', 'four', 'five'}
-- switchValues(table, 2, 4)
table.sort(tabel)
print_r(tabel)
print()

tabel = {'aap', 'noot', 'mies', 'teun', 'zus','wim', 'jet'}
table.sort(tabel, function(a,b) return a<b end)
print_r(tabel)
